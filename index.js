/**
 * @format
 */

import React from 'react';
import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import promiseMiddleware from 'redux-promise';
import reducers from './src/store/reducers';
import { Provider as PaperProvider} from 'react-native-paper';


const componseEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const createStoreWithMiddleware = createStore(
    reducers,
    componseEnhancers(applyMiddleware(promiseMiddleware)))

const reduxApp = () => {
    return (
        <Provider store={createStoreWithMiddleware}>
            {/* <PaperProvider theme={DarkTheme}> */}
            <PaperProvider>
            <App />
            </PaperProvider>
        </Provider>
    )
}

//AppRegistry.registerComponent(appName, () => App);

AppRegistry.registerComponent(appName, () => reduxApp);
