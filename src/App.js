
import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { connect } from 'react-redux';
import AuthScreen from './components/auth';
import { Stack, HomeStack, VideoStack,screenOptions } from './routes/stacks';
import ProfileScreen from './components/user/profile/profile';
import SideDrawerCustom from './utils/customDrawer';
import {Colors} from './utils/tools';
import VideoScreen from '../src/components/home/videos/video';

const Drawer = createDrawerNavigator();

const MainDrawer = () => {
  return (
    <Drawer.Navigator
    drawerContent={(props)=> <SideDrawerCustom {...props}/>}
    drawerStyle={{backgroundColor:Colors.black}}    
   
    >
      <Drawer.Screen name="Home" component={HomeStack} />
      <Drawer.Screen name="Videos" component={VideoStack} />
      <Drawer.Screen name="Profile" component={ProfileScreen}/>
    </Drawer.Navigator>
  )
}



const App = (props) => {
  return (

    <NavigationContainer>
      {/*   Main Stack NAvigator of the App */}
      <Stack.Navigator>

        {props.auth ?

          //whole APP
          //  if user is authenticated we show the Home
          // we will have 1 stack which shows HomeScreen aka Articles, VideosScreen
          // plus on that Stack we want Drawer also
          <>
          <Stack.Screen 
          name="Main"
          options={{headerShown:false}} 
          component={MainDrawer} />
        
          <Stack.Screen name="VideoScreen" component={VideoScreen} 
          options={{...screenOptions}}
          />
      
        </>





          :
          //login ROute && not Authenticated Route
          //  if the app load===> we show login screen
          //  if the user is not authenticated ==> we show the login screen

          <Stack.Screen name="AuthScreen" component={AuthScreen} />
        }






      </Stack.Navigator>
    </NavigationContainer>



  )

}

const mapStateToProps = state => ({ auth: state.auth })

export default connect(mapStateToProps)(App);
