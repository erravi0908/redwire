import {combineReducers} from 'redux';
import auth from './auth_reducers.js';

const rootReducer=combineReducers({
    auth
})

export default rootReducer;