import React from 'react';
import { View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../components/home/articles';
import ArticleScreen from '../components/home/articles/article';
import VideosScreen from '../components/home/videos';
//import VideoScreen from '../components/home/videos/video';
import { Platform } from 'react-native';
import { Icon } from 'react-native-elements';

import { Colors, LogoText } from '../utils/tools';
import { useNavigation } from '@react-navigation/native';

export const Stack = createStackNavigator();


export const screenOptions = {
    headerTitleAlign: 'center',
    headerTintColor: Colors.red,
    headerStyle: {
        backgroundColor: Colors.black,
        borderBottomWidth: 6,
        borderBottomColor: Colors.red,
        height: Platform === "ios" ? 110 : 90
    },

    headerTitle: () => <LogoText style={{ fontSize: 25 }} />,


}

const LeftIcon = () => {

    const navigation = useNavigation();
    return (
        <View style={{ margin: 10 }}>
            <Icon name="menufold" type="antdesign" color={Colors.white}

                onPress={() => { navigation.openDrawer() }} />
        </View>
    )
}


export const HomeStack = () => {
    return (

        <Stack.Navigator initialRouteName="Home_screen"
            screenOptions={{...screenOptions,}}>
                
            <Stack.Screen name="Home_screen" component={HomeScreen} options={{ headerLeft: (props) => <LeftIcon /> }} />
            <Stack.Screen name="Article_screen" component={ArticleScreen} />
        </Stack.Navigator>

    )
}

export const VideoStack = () => {
    return (
        <Stack.Navigator initialRouteName="Videos_screen"
            screenOptions={{ ...screenOptions }} >
            <Stack.Screen name="Videos_screen" component={VideosScreen} options={{ headerLeft: (props) => <LeftIcon /> }} />
        
         
        
        </Stack.Navigator>

    )
}
