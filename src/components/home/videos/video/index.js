import React from 'react';

import { View,ScrollView } from 'react-native';
import ContentShow from '../../../../utils/contentShow';
import YouTube from 'react-native-youtube';

const VideoScreen = () => {

    return (
        <ScrollView>
            <View>
                <YouTube apiKey="AIzaSyBv550WDfpWgKgrQdMADgUZf050zKRYN-Q"
                videoId="qSRrxpdMpVc"
                style={{ alignSelf: 'stretch', height: 300 }}
                play={false}
                onReady={e=>console.log('ready')}
                onChangeState={e=> console.log(e)}
                onError={error=>{console.log(error)}}
                
                
                />

                <ContentShow />
            </View>
        </ScrollView>

    )


}

export default VideoScreen;

