import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { Tile } from 'react-native-elements';
const VideosScreen = ({ navigation }) => {


    const renderVideo = () => {
        return (

            <Tile
                imageSrc={{ uri: "https://picsum.photos/200/300" }}
                title="Video To be Played "
                icon={{ name: 'play-circle', type: 'font-awesome', color: 'red', size: 50 }}
                contentContainerStyle={styles.contentContainerStyle}
                containerStyle={styles.containerStyle}
                titleStyle={{ fontSize: 15 }}
                onPress={()=>{navigation.navigate("VideoScreen", {id:"VideoID", postData:{title:"VideoTitle" ,content:'adadad'}})}}


            />
        )
    }


    return (

        <ScrollView>
            <View style={{ padding:5 }}>
                {renderVideo()}
                {renderVideo()}
                {renderVideo()}
                {renderVideo()}
                {renderVideo()}
                {renderVideo()}
                {renderVideo()}
                {renderVideo()}
                {renderVideo()}

            </View>
        </ScrollView>


    )


}


const styles = StyleSheet.create({

    contentContainerStyle: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#e1e8ee',
        shadowColor: 'rgba(0,0,0,.2)'

    },
    containerStyle: {

        width: '100%', height: 250, marginBottom: 15

    }

})


export default VideosScreen;