import React from 'react';

import { View,ScrollView,Text, TouchableOpacity,StyleSheet } from 'react-native';
import { Card } from 'react-native-elements';

// This will be  the HomeScreen when App loads 
const HomeScreen = ({ navigation }) => {


    // creating a Card, data shall be filled from calling server APIs
    const renderCard = () => {
        return (
            <TouchableOpacity
            
            onLongPress={()=>{navigation.navigate('Article_screen',{id:'sdfsdf',postData:{title:'TT',content:"zxcxc"}})}
        }
            
            >
                <Card>
                    <Card.Title style={styles.cardTitle}>
                        Lorem ipsum is placeholder text commonly used in the graphic, 
                        print, and publishing industries for previewing layouts and visual mockups.
                    </Card.Title>
                    <Card.Divider/>
                    <Text style={styles.cardText}>
                    Lorem ipsum is placeholder text commonly used in the graphic, print,
                     and publishing industries for previewing layouts and visual mockups.
                        
                    </Text>
                </Card>
            </TouchableOpacity>
        )
    }

// Returning the Actual ScrollView with Card inside in
    return (
        <View>
            <ScrollView>
                {renderCard()}
                {renderCard()}
                {renderCard()}
                {renderCard()}
                {renderCard()}
                {renderCard()}
                {renderCard()}
            </ScrollView>
        </View>
    )
}


const styles=StyleSheet.create({

    cardTitle:{
        textAlign:'left',
        fontSize:20
    },
    cardText:{
        marginBottom:10,
        marginTop:10,
       // fontFamily:'Monoton-Regular'
    }

})



export default HomeScreen;