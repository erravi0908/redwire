
import React from 'react';

import {View,ScrollView, ActivityIndicator } from 'react-native';
import {Image} from 'react-native-elements';
import ContentShow from '../../../../utils/contentShow';
const ArticleScreen = () =>
{

return (
    <ScrollView>
    <View>
        <Image source={{uri:'https://picsum.photos/200/300'}} 
        style={{width:350,height:200, marginLeft:5,marginRight:5}}
         PlaceholderContent={<ActivityIndicator/>}/>
         <ContentShow/>
    </View>
    </ScrollView>


)


}

export default ArticleScreen;