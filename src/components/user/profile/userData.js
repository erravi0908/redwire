import React, { useState } from 'react';
import { View } from 'react-native';
import { TextInput, Button, Title } from 'react-native-paper';
import { Formik } from 'formik';
import * as Yup from 'yup';




const UserData = () => {

    const handleSubmitForm = (values) => { alert("Submit Clicked") }

    return (
        <View>
            <Formik enableReinitialize={true}
                initialValues={{ name: '', lastname: '', age: '' }}

                validationSchema={
                    Yup.object(
                        {
                            name: Yup.string().required('The Name is required')
                            , lastname: Yup.string().required('The LastName is required')
                            , age: Yup.number().required('The Age is required')

                        })



                }

                onSubmit={values => handleSubmitForm(values)}

            >

                {({ handleBlur, handleChange, handleSubmit, values, touched, errors }) => (

                    <View style={{ padding: 20 }}>
                        <TextInput
                            label="name" value={''}
                            onChangeText={handleChange}
                            onBlur={handleBlur('name')}
                            error={errors.name && touched.name ? true : false}
                            value={values.name}
                            mode="flat"
                        />
                        <TextInput
                            label="lastname" value={''}
                            onChangeText={handleChange}
                            onBlur={handleBlur('lastname')}
                            error={errors.lastname && touched.lastname ? true : false}
                            value={values.lastname}
                            mode="flat"
                        />
                        <TextInput
                            label="age" value={''}
                            onChangeText={handleChange}
                            onBlur={handleBlur('age')}
                            error={errors.age && touched.age ? true : false}
                            value={values.age}
                            mode="flat"
                        />


                        <Button mode="contained" style={{ marginTop: 5 }}
                            onPress={handleSubmitForm}>Update </Button>
                    </View>

                )}

            </Formik>
        </View>

    )

}

export default UserData;