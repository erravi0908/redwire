import React from 'react';

import { View, StyleSheet } from 'react-native';
import { DrawerContentScrollView } from '@react-navigation/drawer';
import { Button } from 'react-native-elements';
import { Colors, LogoText } from './tools';

// This will be  the HomeScreen when App loads 
const SideDrawerCustom = (props) => {
  
    console.log("Executing SideDrawerCustom Component")
    const mainOptions = [
        { title: 'News', location: "Home" },
        { title: 'Videos', location: "Videos" },
        { title: 'Profile', location: "Profile" }


    ]

    return (
        <DrawerContentScrollView>
            {/* // Logo Component Style */}
            <View>
                <LogoText />
            </View>
            { mainOptions.map((item) => (
                <Button
                    key={item.location}
                    title={item.title}
                    buttonStyle={styles.drawerButton}
                    titleStyle={{ width: '100%' }}
                    onPress={() => { props.navigation.navigate(item.location) }}
                />
            ))}

            <Button

                title="Logout"
                buttonStyle={styles.drawerButton}
                titleStyle={{ width: '100%' }}
                onPress={() => { alert("You are Going to Logout") }}
            />

        </DrawerContentScrollView>


    )


}


const styles = StyleSheet.create({

    drawerButton: {
        backgroundColor: Colors.black,
        borderBottomWidth: 1,
        borderBottomColor: Colors.black2
    }

})

export default SideDrawerCustom;