import React from 'react';
import { Text,StyleSheet } from 'react-native';
export const Colors = {

    'white': '#ffffff',
    'black': '#131418',
    'black2': '#272930',
    'black3': '#1a1a21',
    'grey': '#c8c8c8',
    'red': '#d74444'

}


export const LogoText = (props) => {
    return (
        <Text style={{
            fontFamily: 'Monoton-Regular',
            color: '#fefefe',
            fontSize: 30,
            textAlign:'center',
        ...props.style} }
        
        > RED WIRE</Text>
    )
}

const styles=StyleSheet.create({

    textStyle:{
        fontFamily: 'Monoton-Regular',
        color: '#ffffff',
        fontSize: 50
        
    
    }

})

